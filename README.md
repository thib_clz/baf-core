# baf-core


# Building the project

To build the project, simply install <a href = https://doc.rust-lang.org/cargo/getting-started/installation.html > Cargo </a> and type `Cargo build (--release)` in the root of the project. 

# Use the program
- Run the program compiled where you want to index files
- A `.baf` folder should appear, this folder contains 3 json files :
  - index.json: contains summary of the content of the folder
  - files.json: contains the list of files in the folder as well as some metadata
  - folder.json: contains the list of folders contained inside the main folder as well as the folder itself


# TODO :
- Add an efficient methode of updating the files (right now everything has to be reconmputed)
- Adapt the script for Unix OS
- Deploy a CI/CD
- Optimize the current program

