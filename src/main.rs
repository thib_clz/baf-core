use std::env;
use std::fs;
use std::str::FromStr;
use chrono::{DateTime, Utc};
use sha256::try_digest;
use std::path::Path;
use uint::construct_uint;
use serde::{Deserialize, Serialize, Deserializer, Serializer};
use chrono::serde::ts_seconds;


construct_uint! {
    pub struct U256(4);
}

impl Serialize for U256 {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let hex_string = format!("{:x}", self);
        serializer.serialize_str(hex_string.as_str())
    }
}

impl<'de> Deserialize<'de> for U256 {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let hex_string = String::deserialize(deserializer)?;
        U256::from_str(hex_string.as_str()).map_err(serde::de::Error::custom)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Folder {
    name: String,
    key: U256,
    files: Vec<(String, U256)>,
    folders: Vec<Folder>,
}

impl Folder {
    pub fn new(file_path: &str, list_of_files: &mut Vec<File>, list_of_folders: &mut Vec<(String, U256)>) -> Self {
        let (files, folders) = Folder::index_content(file_path, list_of_files, list_of_folders);
    
        let files_summary: Vec<(String, U256)> = files.iter().map(|x| (x.name.clone(), x.key)).collect();
        let folders_summary: Vec<(String, U256)> = folders.iter().map(|x| (x.name.clone(), x.key)).collect();
        let mut result: U256 = files_summary.iter().fold(U256::from(0), |acc, x| acc ^ x.1);
        result = folders_summary.iter().fold(result, |acc, x| acc ^ x.1);

        list_of_files.extend(files);
        list_of_folders.extend(folders_summary);

        Folder {
            name: file_path.to_string(),
            key: result,
            files: files_summary,
            folders,
        }
    }

    
    fn index_content(file_path: &str, list_of_files: &mut Vec<File>, list_of_folders: &mut Vec<(String, U256)>) -> (Vec<File>, Vec<Folder>) {
        let mut files: Vec<File> = Vec::new(); 
        let mut folders: Vec<Folder> = Vec::new();
        for entry in fs::read_dir(file_path).unwrap() {
            let path = entry.unwrap().path();
            let metadata = fs::metadata(&path).unwrap();
            if path.file_name().unwrap().to_str().unwrap() != ".baf" {
                if metadata.is_dir() {
                    folders.push(Folder::handle_folder(&path, list_of_files, list_of_folders));
                }
                else {
                    files.push(Folder::handle_file(metadata, &path));
                }
            }
        }
        (files, folders)
    }

    fn handle_file(metadata: fs::Metadata, path: &Path) -> File {
        let ctime = DateTime::from(metadata.created().unwrap());
        let mtime = DateTime::from(metadata.modified().unwrap());
        let size = metadata.len();
        File::new(path.to_str().unwrap(), ctime, mtime, size)
    }

    fn handle_folder(path: &Path, list_of_files: &mut Vec<File>, list_of_folders: &mut Vec<(String, U256)>) -> Folder {
        let folder_path = path.to_str().unwrap();
        Folder::new( folder_path, list_of_files, list_of_folders)
    }
        
    fn save_to_file(&self, file_path: &str) {
        let serialized = serde_json::to_string_pretty(&self).unwrap();
        let baf_path = format!("{}/.baf", file_path);
        let directory = Path::new(baf_path.as_str());
        if !directory.exists() {
            let _ = fs::create_dir_all(directory);
        }

        fs::write(format!("{}/.baf/index.json", file_path), serialized).expect("Unable to write file");
    }

}

impl Clone for Folder {
    fn clone(&self) -> Self {
        Self {
            name: self.name.clone(),
            key: self.key,
            files: self.files.clone(),
            folders: self.folders.clone(),
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct File {
    pub name: String,
    pub key: U256,
    #[serde(with = "ts_seconds")]
    pub ctime: DateTime<Utc>,
    #[serde(with = "ts_seconds")]
    pub mtime: DateTime<Utc>,
    pub size: u64,
}

impl File {
    pub fn new(file_path: &str, ctime: DateTime<Utc>, mtime: DateTime<Utc>, size: u64) -> Self {
        Self {
            name: file_path.to_string(),
            key: File::calculate_checksum(file_path),
            ctime,
            mtime,
            size,
        }
    }
    fn calculate_checksum(file_path: &str) -> U256 {
        let file = Path::new(file_path);
        U256::from_str(try_digest(file).unwrap().as_str()).unwrap()
    }

}

impl Clone for File {
    fn clone(&self) -> Self {
        Self {
            name: self.name.clone(),
            key: self.key,
            ctime: self.ctime,
            mtime: self.mtime,
            size: self.size,
        }
    }
}

fn main() {
    if let Ok(current_dir) = env::current_dir() {
        let mut files_list: Vec<File> = Vec::new();
        let mut folders_list: Vec<(String, U256)> = Vec::new();

        let folder = Folder::new(  current_dir.to_str().unwrap(),  &mut files_list, &mut folders_list);

        folder.save_to_file(current_dir.to_str().unwrap());
        folders_list.push((folder.name.clone(), folder.key));

        fs::write(format!("{}/.baf/files.json", current_dir.to_str().unwrap().to_string()), serde_json::to_string_pretty(&files_list).unwrap()).expect("Unable to write file");
        fs::write(format!("{}/.baf/folders.json", current_dir.to_str().unwrap().to_string()), serde_json::to_string_pretty(&folders_list).unwrap()).expect("Unable to write file");

    } else {
        eprintln!("Failed to get current directory");
    }
}
